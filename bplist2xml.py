import os

import bplistlib
import plistlib
import getopt

import sys

opts, args = getopt.getopt(sys.argv[1:], 'x:y:')


try:
    inputfile = args[0]
except IndexError:
    inputfile = ""
    print "Usage: bplist2xml (type) (input) [output]"
    sys.exit()
try:
    outputfile = args[1]
except IndexError:
    outputfile = inputfile

if not os.path.exists(inputfile):
    print "File does not exist."
    sys.exit()

print "Converting..."
plist = bplistlib.readPlist(inputfile)
bplistlib.writePlist(plist,outputfile,binary=False)
print "Done!"


