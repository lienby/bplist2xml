# bplist2xml
Convert Binary PLIST to XML Plist 
For windows and linux.

Usage: bplist2xml (path) [output]

Win32: https://bitbucket.org/SilicaAndPina/bplist2xml/downloads/bplist2xml-win32.zip  
Linux64: https://bitbucket.org/SilicaAndPina/bplist2xml/downloads/bplist2xml-linux.tar.gz  